$(function () {
  $(document).ready(function(){
    $('.cpf').mask('000.000.000-00', {reverse: true});
    $('.telefone').mask('(00) 0000-0000');

    var scntDiv = $('#dynamicDiv');

    $("#addInput").unbind().click(function(event){
      console.log('teste')
        $('<p>'+
          '<input type="text" class="telefone" name="telefone[]" size="20" value="" placeholder="" /> '+
          '<a class="btn btn-danger" href="javascript:void(0)" id="remInput">'+
        '<span class="glyphicon glyphicon-minus" aria-hidden="true"></span> '+
        'Remover Telefone'+
          '</a>'+
    '</p>').appendTo(scntDiv);
        return false;
    });

    $(document).on('click', '#remInput', function () {
          $(this).parents('p').remove();
        return false;
    });
    
  });
});