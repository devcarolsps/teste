<?php

class Clients extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('clients_model');
        $this->load->helper('url_helper');
        $this->load->library('session');
        $this->load->library('pagination');
    }

    public function index() {
        if (!$this->session->userdata('is_logged_in')) {
            redirect(site_url('users/login'));
        } else {
            $data['user_id'] = $this->session->userdata('user_id');
        }
       

        $data['clients'] = $this->clients_model->get_clients();

        foreach ($data['clients'] as $clients_item){
            $idCliente = $clients_item['id'];
            $data['telefones'] = $this->clients_model->get_telefones_by_clients($idCliente);
        }

        
        $data['title'] = 'Clientes';

        $this->load->view('templates/admin-header', $data);
        $this->load->view('templates/admin-sidebar');
        $this->load->view('clients/index', $data);
        $this->load->view('templates/footer');
    }

    public function view($nome = NULL) {
        if (!$this->session->userdata('is_logged_in')) {
            redirect(site_url('users/login'));
        } else {
            $data['user_id'] = $this->session->userdata('user_id');
        }
        $data['clients_item'] = $this->clients_model->get_clients($nome);

        if (empty($data['clients_item'])) {
            show_404();
        }

        $data['title'] = $data['clients_item']['title'];

        $this->load->view('templates/admin-header', $data);
        $this->load->view('templates/admin-sidebar');
        $this->load->view('clients/view', $data);
        $this->load->view('templates/footer');
    }

    public function create() {
        if (!$this->session->userdata('is_logged_in')) {
            redirect(site_url('users/login'));
        } else {
            $data['user_id'] = $this->session->userdata('user_id');
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['title'] = 'Adicionar Cliente';

        $this->form_validation->set_rules('nome', 'Nome do Cliente', 'required');
        $this->form_validation->set_rules('cpf', 'CPF do Cliente', 'required');

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('templates/admin-header', $data);
            $this->load->view('templates/admin-sidebar');
            $this->load->view('clients/create');
            $this->load->view('templates/footer');
        } else {
            $this->clients_model->insert_clients();
            redirect(site_url('clients'));
        }
    }

    public function edit() {
        if (!$this->session->userdata('is_logged_in')) {
            redirect(site_url('users/login'));
        } else {
            $data['user_id'] = $this->session->userdata('user_id');
        }

        $id = $this->uri->segment(3);

        if (empty($id)) {
            show_404();
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['title'] = 'Editar Cliente';
        $data['clients_item'] = $this->clients_model->get_clients_by_id($id);
        $data['telefones'] = $this->clients_model->get_telefones_by_clients();
        


        if ($data['clients_item']['user_id'] != $this->session->userdata('user_id')) {
            $currentClass = $this->router->fetch_class(); // class = controller
            redirect(site_url($currentClass));
        }

        

        $this->form_validation->set_rules('nome', 'Nome', 'required');
        $this->form_validation->set_rules('cpf', 'CPF', 'required');

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('templates/admin-header', $data);
            $this->load->view('templates/admin-sidebar');
            $this->load->view('clients/edit', $data);
            $this->load->view('templates/footer');
        } else {
            $this->clients_model->set_clients($id);
            redirect(site_url('clients'));
        }
    }

    public function delete($id) {
        if (!$this->session->userdata('is_logged_in')) {
            redirect(site_url('users/login'));
        }

        //$id = $this->uri->segment(3);

        if (empty($id)) {
            show_404();
        }

        $clients_item = $this->clients_model->get_clients_by_id($id);

        if ($clients_item['user_id'] != $this->session->userdata('user_id')) {
            $currentClass = $this->router->fetch_class(); // class = controller
            redirect(site_url($currentClass));
        }

        $this->clients_model->delete_clients($id);
        //redirect(base_url() . '/clients');
    }

}
