<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-10">
            <h3 class="page-header"><?php echo $title; ?></h3>
        </div>
        <div class="col-lg-2">
            <a href="<?php echo site_url('clients'); ?>" class="page-header btn btn-success pull-right">Listar Clientes</a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading"><?php echo $title; ?></div>
                <?php $attributes = array("name" => "clientsform", "role" => "form"); ?>
                <?php echo form_open_multipart('clients/create', $attributes); ?>
                <div class="panel-body">
                    <div class="form-group <?php echo form_error('nome') ? 'has-error' : '' ?>">
                        <label>Nome Cliente</label>
                        <input class="form-control " name="nome" type="text" value=""/>
                    </div>

                    <div class="form-group <?php echo form_error('cpf') ? 'has-error' : '' ?>"">
                        <label>CPF Cliente</label>
                        <input class="form-control cpf" name="cpf" type="text" value=""/>
                    </div>

                    <div class="form-group">
                        <label>E-mail Cliente</label>
                        <input class="form-control" name="email" type="text" value=""/>
                    </div>
                    <hr>
                    <div class="form-group">
                        <label>Telefones</label>
                        <a class="btn btn-primary" href="javascript:void(0)" id="addInput">
                            <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                            Adicionar Telefone
                        </a>
                        <br/>
                        <br/>
                        <div id="dynamicDiv">
                            <p>
                                <input type="text" class="" maxlength="11"  name="telefone[]" size="20" value="" placeholder="DDD+NUM" />
                                <a class="btn btn-danger" href="javascript:void(0)" id="remInput">
                                    <span class="glyphicon glyphicon-minus" aria-hidden="true"></span>
                                    Remover Telefone
                                </a>
                            </p>
                        </div>

                    </div>
                </div>
                <input type="hidden" name="user_id" value="<?php echo $user_id; ?>"/>
                <button type="submit" name="submit" class="btn btn-success">Salvar</button>
                <a href="<?php echo site_url('clients'); ?>" class="btn btn-warning">Voltar</a>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>