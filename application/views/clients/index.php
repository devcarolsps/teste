<?php

function formatarCPF_CNPJ($campo, $formatado=TRUE)
{
      # retira formato
      $codigoLimpo = preg_replace("[' '-./ t]", '', $campo);
      
      # pega o tamanho da string menos os digitos verificadores
      $tamanho = (strlen($codigoLimpo) -2);
      
      # verifica se o tamanho do código informado é válido
      if ($tamanho != 9 && $tamanho != 12)
      {
          return FALSE;
      }

      if ($formatado)
      {
          # seleciona a máscara para cpf ou cnpj
          $mascara = ($tamanho == 9) ? '###.###.###-##' : '##.###.###/####-##';

          $indice = -1;
          for ($i=0; $i < strlen($mascara); $i++)
          {
              if ($mascara[$i]=='#') $mascara[$i] = $codigoLimpo[++$indice];
          }
          
          #retorna o campo formatado
          $retorno = $mascara;
      }
      else
      {
          //se não quer formatado, retorna o campo limpo
          $retorno = $codigoLimpo;
      }
      return $retorno;

}
?>

<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-10">
            <h3 class="page-header"><?php echo $title; ?></h3>
        </div>
        <div class="col-lg-2">
            <a href="<?php echo site_url('clients/create'); ?>" class="page-header btn btn-success pull-right">Add Cliente</a>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo $title; ?>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>nome</th>
                                <th>cpf</th>
                                <th>e-mail</th>
                                <th>Data Criação</th>
                                <th>Telefones</th>
                                <th>Ação</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $i = 1; ?>
                            <?php foreach ($clients as $clients_item): ?>
                                <tr>
                                    <td><?php echo $clients_item['id']; ?></td>
                                    <td><?php echo $clients_item['nome']; ?></td>
                                    <td><?php echo formatarCPF_CNPJ($clients_item['cpf']); ?></td>
                                    <td><?php echo $clients_item['email']; ?></td>
                                    <td><?php echo date('d/m/Y H:i:s',strtotime($clients_item['dt_criacao']))  ?></td>
                                    <td>
                                        
                                        <?php foreach ($telefones as $telefone_item): ?>
                                            <?php if ($telefone_item['clients_id'] == $clients_item['id']) { ?>
                                                <?php echo $telefone_item['telefone']  ?> <br>
                                            <?php }  ?>                                    
                                        <?php endforeach; ?>
                                    </td>
                                    <td>
                                        <?php if ($this->session->userdata('is_logged_in')) { ?>
                                            <a class="btn btn-success btn-circle" title="Editar" href="<?php echo site_url('clients/edit/' . $clients_item['id']); ?>"><i class="fa fa-edit"></i></a> |
                                            <a class="btn btn-danger btn-circle remove" title="Excluir" ?>
                                               <i class="fa  fa-trash-o"></i></a>
                                        <?php }  ?>
                                    </td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js" type="text/javascript"></script>

<script>
$(".remove").click(function(){

    var id = $(this).closest("tr").find('td').eq(0).text();
    console.log("<?php echo base_url(); ?>application/controllers/Clients/delete/"+id)


    swal({

        title: "Deseja Excluir?",
        text: "Você não podera recuperar esses dados novamente!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: "Sim, deletar!",
        cancelButtonText: "Não, Cancelar!",
        closeOnConfirm: false,
        closeOnCancel: false
    },
    function(isConfirm) {
    if (isConfirm) {
        $.ajax({
            url: "clients/delete/"+id,
            type: 'POST',
            beforeSend: function() {
                try 
                {
                } 
                catch (ex) {
                    console.log("Erro!" , "AJAX Error (beforeSend):\n" + ex, 'error');
                }
            },
            success: function(response) {
                console.log(response)
                swal("Deletado!", "Deletado com Sucesso", "success");
                window.location.href = "https://sertosys.com.br/teste/clients"
            },
            error: function(error) {
                try 
                {
                    console.log("Erro!" , "PHP Error (error):\r\n" + JSON.stringify(error), 'error');
                } catch (ex) 
                {
                    console.log("Erro!" , "AJAX Error (error):\n" + JSON.stringify(ex), 'error');
                }

            }
        });
    } 
    else 
    {
        swal("Cancelado", "Operação cancelada :)", "error");
    }

    });   
    

});    

</script>