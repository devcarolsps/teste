<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-10">
			<h3 class="page-header"><?php echo $title; ?></h3>
		</div>
		<div class="col-lg-2">
			<a href="<?= site_url('clients'); ?>" class="page-header btn btn-success pull-right">Listar Clients</a>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<?php echo validation_errors(); ?>
			<div class="panel panel-default">
				<div class="panel-heading"><?php echo $title; ?></div>
				<div class="panel-body">
					<?php $attributes = array("name" => "clientsform", "role" => "form"); ?>
					<?php echo form_open_multipart('clients/edit/'.$clients_item['id']); ?>
					<div class="form-group">
						<label>Nome</label>
						<input class="form-control" name="nome" type="text" value="<?= $clients_item['nome'] ?>"/>
					</div>

					<div class="form-group">
						<label>CPF</label>
						<input class="form-control cpf"  name="cpf" type="text" value="<?= $clients_item['cpf'] ?>"/>
					</div>

					<div class="form-group">
						<label>E-mail</label>
						<input class="form-control" name="email" type="text" value="<?= $clients_item['email'] ?>"/>
					</div>

					<div class="form-group">
						<label>Data Criação</label>
						<input class="form-control" disabled name="dtCriacao" type="text" value="<?= date('d/m/Y H:i:s' , strtotime($clients_item['dt_criacao']) )  ?>"/>
					</div>
					<hr>
					<?php foreach ($telefones as $telefone_item): ?>
						<?php if ($telefone_item['clients_id'] == $clients_item['id']) { ?>
							<div class="form-group">
								<label>Telefones</label>
								<input class="form-control" name="telefone[]" type="text" value="<?= $telefone_item['telefone']   ?>"/>
							</div>
						<?php }  ?>  
					<?php endforeach; ?>

					<input type="hidden" name="user_id" value="<?php echo $user_id; ?>"/>
					<button type="submit" name="submit" class="btn btn-success">Atualizar</button>
					<a href="<?php echo site_url('clients'); ?>" class="btn btn-warning">Voltar</a>
					<?php echo form_close(); ?>
				</div>
			</div>
		</div>
	</div>
</div>



