<?php
class Clients_model extends CI_Model {
 
    public function __construct()
    {
        $this->load->database();
    }
    
    public function record_count()
    {
        return $this->db->count_all('clients');
    }
    
    public function get_clients($nome = FALSE)
    {
        $userId = $this->session->userdata('user_id'); 
        if ($nome === FALSE)
        {
            $this->db->select('c.id, c.nome, c.cpf, c.email, c.dt_criacao, c.dt_alteracao, c.dt_exclusao');
            $this->db->from('clients c');
            //$this->db->join('telefones t ', 't.clients_id = c.id', 'left');        
            $this->db->where('c.user_id = ', $userId);   
            $this->db->where('c.dt_exclusao IS NULL');            
            $query = $this->db->get();

            return $query->result_array(); 
        }
 
        $this->db->select('c.id, c.nome, c.cpf, c.email, c.dt_criacao, c.dt_alteracao, c.dt_exclusao');
        $this->db->from('clients c');
        //$this->db->join('telefones t ', 't.clients_id = c.id', 'left');        
        $this->db->where('c.user_id = ', $userId);   
        $this->db->where('c.nome = ', $nome);   
        $this->db->where('c.dt_exclusao IS NULL');            
        $query = $this->db->get();

        return $query->row_array();
    }

    public function get_telefones_by_clients()
    {         
        //var_dump($idCliente);
        $this->db->select('t.telefone, t.clients_id');
        $this->db->from('telefones t');      
        //$this->db->where('t.clients_id = ', 3); 
        $query = $this->db->get();

        return $query->result_array(); 
    }
    
        
    public function get_clients_by_id($id = 0)
    {
        if ($id === 0)
        {
            $query = $this->db->get('clients');
            return $query->result_array();
        }
 
        $query = $this->db->get_where('clients', array('id' => $id));
        return $query->row_array();
    }
    
    public function set_clients($id = 0)
    {
        $cpf = $this->input->post('cpf');
        $cpf = trim($cpf);
        $cpf = str_replace(".", "", $cpf);
        $cpf = str_replace(",", "", $cpf);
        $cpf = str_replace("-", "", $cpf);

        $data = array(
            'nome' => $this->input->post('nome'),
            'cpf' => $cpf,
            'email' => $this->input->post('email'),
            'user_id' => $this->input->post('user_id'),
            'dt_alteracao' => date('Y-m-d H:i:s')
        );
        
        if ($id == 0) {
            return $this->db->insert('clients', $data);
        } else {
            $this->db->where('id', $id);
            return $this->db->update('clients', $data);
        }
    }

    public function insert_clients()
    {        
        $cpf = $this->input->post('cpf');
        $cpf = trim($cpf);
        $cpf = str_replace(".", "", $cpf);
        $cpf = str_replace(",", "", $cpf);
        $cpf = str_replace("-", "", $cpf);

        
        
        $data = array(
            'nome' => $this->input->post('nome'),
            'cpf' => $cpf,
            'email' => $this->input->post('email'),
            'user_id' => $this->input->post('user_id'),
            'dt_criacao' => date('Y-m-d H:i:s')
        );  
         
        $this->db->insert('clients', $data);
        $this->db->insert_id();

        $telefoneArray = $this->input->post('telefone');
        if(!empty($telefoneArray))
        {
            $data2 = array();
            foreach($telefoneArray as $row){
                $data2[] = array(
                    'clients_id'      => $this->db->insert_id(),
                    'telefone'     => $row,
                );
            }
    
            $this->db->insert_batch('telefones', $data2);
        }
        
           
    }
    
    
    public function delete_clients($id)
    {
        $data = array(
            'dt_exclusao' => date('Y-m-d H:i:s')
        );

        $this->db->where('id', $id);
        return $this->db->update('clients', $data); 
    }
}
